import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JcorianListComponent } from './jcorian-list.component';

describe('JcorianListComponent', () => {
  let component: JcorianListComponent;
  let fixture: ComponentFixture<JcorianListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JcorianListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JcorianListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
