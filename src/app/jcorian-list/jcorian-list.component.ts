import { Component, OnInit } from '@angular/core';
import {Jcorian} from '../models/jcorian';
import { JcorianServiceService } from '../service/jcorian-service.service';


@Component({
  selector: 'app-jcorian-list',
  templateUrl: './jcorian-list.component.html',
  styleUrls: ['./jcorian-list.component.scss']
})
export class JcorianListComponent implements OnInit {

  jcorians: Jcorian[];

  constructor(private JcorianService: JcorianServiceService) {
  }

  ngOnInit() {
    this.JcorianService.findAll().subscribe(data => {
      this.jcorians = data;
    });
  }
}
