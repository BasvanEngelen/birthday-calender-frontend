import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JcorianListComponent } from './jcorian-list/jcorian-list.component';
import { JcorianFormComponent } from './jcorian-form/jcorian-form.component';
import {JcorianServiceService} from './service/jcorian-service.service';
import {FormsModule} from '@angular/forms';
import { JcorianBirthdayOverviewComponent } from './jcorian-birthday-overview/jcorian-birthday-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    JcorianListComponent,
    JcorianFormComponent,
    JcorianBirthdayOverviewComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule
    ],
  providers: [JcorianServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
