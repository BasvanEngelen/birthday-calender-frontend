import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Jcorian} from '../models/jcorian';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()

export class JcorianServiceService {

   private jcorianUrl: string;
   private queryString: string;

  constructor(private http: HttpClient) {
    this.jcorianUrl = environment.jcorianBaseUrl + '/jcorians';
  }

  public findAll(): Observable<Jcorian[]> {
    return this.http.get<Jcorian[]>(this.jcorianUrl);
  }

  public save(jcorian: Jcorian) {
    return this.http.post<Jcorian>(this.jcorianUrl, jcorian);
  }

public getByDayAndMonth(day: string, month: string): Observable<Jcorian[]> {
    this.queryString = this.jcorianUrl + '?day=' + day + '&month=' + month;
    return this.http.get<Jcorian[]>(this.queryString);
}
}
