import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JcorianServiceService } from '../service/jcorian-service.service';
import { Jcorian } from '../models/jcorian';


@Component({
  selector: 'app-jcorian-form',
  templateUrl: './jcorian-form.component.html',
  styleUrls: ['./jcorian-form.component.scss']
})
export class JcorianFormComponent implements OnInit {

  jcorian: Jcorian;
  constructor( private route: ActivatedRoute,
               private router: Router,
               private jcorianServiceService: JcorianServiceService) {
    this.jcorian = new Jcorian();
  }


  onSubmit() {
    this.jcorianServiceService.save(this.jcorian).subscribe(result => this.goToJcorianList());
  }

  goToJcorianList() {
    this.router.navigate(['/jcorians']);
  }

  ngOnInit(): void {
  }


}
