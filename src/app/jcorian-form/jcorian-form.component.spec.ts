import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JcorianFormComponent } from './jcorian-form.component';

describe('JcorianFormComponent', () => {
  let component: JcorianFormComponent;
  let fixture: ComponentFixture<JcorianFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JcorianFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JcorianFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
