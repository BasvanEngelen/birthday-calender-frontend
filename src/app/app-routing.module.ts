import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JcorianListComponent } from './jcorian-list/jcorian-list.component';
import { JcorianFormComponent } from './jcorian-form/jcorian-form.component';
import {JcorianBirthdayOverviewComponent} from './jcorian-birthday-overview/jcorian-birthday-overview.component';



const routes: Routes = [
  { path: 'jcorians', component: JcorianListComponent },
  { path: 'addjcorian', component: JcorianFormComponent },
  { path: 'findjcorian', component: JcorianBirthdayOverviewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
