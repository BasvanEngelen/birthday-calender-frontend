import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JcorianBirthdayOverviewComponent } from './jcorian-birthday-overview.component';

describe('JcorianBirthdayOverviewComponent', () => {
  let component: JcorianBirthdayOverviewComponent;
  let fixture: ComponentFixture<JcorianBirthdayOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JcorianBirthdayOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JcorianBirthdayOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
