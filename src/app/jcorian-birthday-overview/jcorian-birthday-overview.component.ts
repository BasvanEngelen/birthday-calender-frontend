import { Component, OnInit } from '@angular/core';
import {Jcorian} from '../models/jcorian';
import { JcorianServiceService } from '../service/jcorian-service.service';


@Component({
  selector: 'app-jcorian-birthday-overview',
  templateUrl: './jcorian-birthday-overview.component.html',
  styleUrls: ['./jcorian-birthday-overview.component.scss']
})
export class JcorianBirthdayOverviewComponent implements OnInit {

    jcorians: Jcorian[];
    day: string;
    month: string;

    constructor(private JcorianService: JcorianServiceService){
  }

  ngOnInit() {}

    onSubmit() {
        this.JcorianService.getByDayAndMonth(this.day, this.month).subscribe(
            data => {
                this.jcorians = data;
            });
    }
}

